#!/usr/bin/env python
from setuptools import setup, find_packages


entry_points = {'console_scripts': [
    'banjo = banjo.bin:main',
]}


setup(
    name="banjo",
    version="0.1.0",
    packages=find_packages(exclude=["tests"]),
    zip_safe=False,
    author="Sergey Blinov",
    author_email="blinovsv@gmail.com",
    install_requires=[
        "PyYAML",
    ],
    entry_points=entry_points,
    url="",
    description="",
    long_description="",
)
