import time
import logging


class Server(object):
    def __init__(self, app):
        self.app = app
        self.logger = logging.getLogger(app)
        self.delay = 1
        self._can_loop = True
        self._sleeping = False

    def setup(self):
        self.logger.info("Setup server")

    def loop(self):
        self.logger.debug("Loop...")

    def cleanup(self):
        self.logger.info("Cleanup server")

    def sleep(self):
        self._sleeping = True
        time.sleep(self.delay)
        self._sleeping = False

    def soft_stop(self):
        if self._sleeping:
            exit(0)
        self._can_loop = False
        self.logger.info("Finalize last loop")

    def sigterm_handler(self, signum, frame):
        self.logger.info("SIGTERM")
        self.soft_stop()

    def start(self):
        self.setup()

        try:
            while self._can_loop:
                self.loop()
                self.sleep()
        except KeyboardInterrupt:
            self.logger.warning("Keyboard interruption: "
                                "use SIGTERM for safe stop")
        finally:
            self.cleanup()
