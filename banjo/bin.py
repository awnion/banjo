import sys
import signal
import logging
import argparse
import importlib

from banjo import settings
from banjo.utils.log import numeric_level


__all__ = ['main']


def main():
    sys.path.insert(0, '.')

    parser = argparse.ArgumentParser(description="Simple loop-worker wrapper")
    parser.add_argument('-a', metavar='app', required=True,
                        help="application (e.g. 'foo.bar.app')")
    parser.add_argument('-s', metavar='settings', default="../secrets.json",
                        help="path to settings (default: '../secrets.json')")
    parser.add_argument('-l', metavar='log_level', default="INFO",
                        choices=['DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'],
                        help="log level (default: INFO)")

    args = parser.parse_args()

    # setup log
    # TODO: use settings for configure logging
    logging.basicConfig(level=numeric_level(args.l))

    # setup config
    settings.set_config_path(args.s)

    # app
    app = args.a
    server_module = importlib.import_module('.server', app)

    server = server_module.Server(app)
    signal.signal(signal.SIGTERM, server.sigterm_handler)
    server.start()
