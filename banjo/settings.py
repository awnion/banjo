import os
import yaml


CONFIG_PATH_ENV = 'CONFIG'


def set_config_path(config_path):
    os.environ[CONFIG_PATH_ENV] = config_path


def get_config():
    config_file = os.environ[CONFIG_PATH_ENV]

    with open(config_file) as f:
        data = f.read()
        if config_file.endswith('.json'):
            data = data.replace('\t', '')

    return yaml.load(data)
