import logging


def numeric_level(name):
    result = getattr(logging, name.strip().upper(), None)
    if not isinstance(result, int):
        raise ValueError('Invalid log level: %s' % name)
    return result
