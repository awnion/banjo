# Banjo #

Simple loop-like application runner.

Solve following problems

* multiple scripts (entry points) in project root

without banjo:

```
#!bash

./my_worker1.py
./my_server2.py
...
```

with banjo: 
```
#!bash

banjo -a my_project.worker.solver
banjo -a my_project.api.server
...
```


* instead of ``logging.basicConfig(...)`` in every entry point just use ``banjo -l DEBUG`` (``INFO`` by default)

* handle ``SIGTERM``: exit after and of current loop (aka safe kill)